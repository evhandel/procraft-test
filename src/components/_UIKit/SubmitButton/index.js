import React from 'react'
import './style.css'

function SubmitButton() {
    return (
        <input
            type="submit"
            value="Зарегистрироваться"
            className="submit-button"
        />
    )
}

export default SubmitButton