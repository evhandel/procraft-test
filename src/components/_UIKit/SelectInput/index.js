import React from 'react'
import PropTypes from 'prop-types'
import ReactSelect from 'react-select'
import 'react-select/dist/react-select.css'
import './style.css'

function SelectInput (props) {

    const {value, handleChange, label} = props
    const options = [{
        label: 'Парикмахер',
        value: 'barber'
    }, {
        label: 'Портной',
        value: 'tailor'
    },{
        label: 'Парковщик',
        value: 'parkingman'
    },{
        label: 'Паркурщик',
        value: 'parkourman'
    },{
        label: 'Дипломат',
        value: 'diplomat'
    },{
        label: 'Визажист',
        value: 'visagiste'
    }]
    return (<div>
        <label>
            {label}<br />
            <ReactSelect
                placeholder=""
                options={options}
                value={value}
                onChange={handleChange}
            />
        </label>
    </div>)
}

SelectInput.defaultProps = {
    label: 'Label'
}

SelectInput.propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    handleChange: PropTypes.func.isRequired
}

export default SelectInput