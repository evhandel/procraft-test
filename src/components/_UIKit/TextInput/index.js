import React from 'react'
import PropTypes from 'prop-types'
import './style.css'

function TextInput(props) {
    const {label, handleChange, value} = props
    return (
        <div className="text-input-component">
            <label>
                {label}<br />
                <input
                    type="text"
                    value={value}
                    onChange={handleChange}
                />
            </label>
        </div>
    )
}

TextInput.defaultProps = {
    label: 'Label'
}

TextInput.propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    handleChange: PropTypes.func.isRequired
}


export default TextInput