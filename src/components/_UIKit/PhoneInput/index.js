import React from 'react'
import PropTypes from 'prop-types'
import ReactTelInput from 'react-telephone-input'
import 'react-telephone-input/css/default.css'
import './style.css'
import flags from './flags.png'

function PhoneInput (props) {
    const {value,handleChange,label} = props
    return (<div>
        <label>
            {label}<br />
            <ReactTelInput
                flagsImagePath = {flags}
                defaultCountry="ru"
                preferredCountries={['ru', 'ua', 'by']}
                value = {value}
                onChange={handleChange}
            />
        </label>
    </div>)
}

PhoneInput.defaultProps = {
    label: 'Label'
}

PhoneInput.propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    handleChange: PropTypes.func.isRequired
}

export default PhoneInput