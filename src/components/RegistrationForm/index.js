import React from 'react'
import './style.css'
import TextInput from '../_UIKit/TextInput'
import PhoneInput from '../_UIKit/PhoneInput'
import SelectInput from '../_UIKit/SelectInput'
import SubmitButton from '../_UIKit/SubmitButton'

class RegistrationForm extends React.Component {

    state = {
        firstName: '',
        secondName: '',
        profession: '',
        phone: '+7'
    }

    handleChangeFor = (propertyName) => (ev) => {

        switch (propertyName) {
            case 'phone': {
                this.setState({'phone': ev})
                break
            }
            case 'profession': {
                this.setState({'profession': ev ? ev.value : ''})
                break
            }
            default: {
                this.setState({[propertyName]: ev.target.value})
            }
        }
    }

    handleSubmit = ev => {
        ev.preventDefault()
        console.log('Form data', this.state)
    }

    render() {
        return (
            <form
                onSubmit={this.handleSubmit}
                className="reg-form">
                <h2><b>Зарегистрируйтесь</b> и начните продавать услуги через интернет сегодня</h2>
                <div className="reg-form--element reg-form--element__first-name">
                    <TextInput
                        label="ИМЯ"
                        handleChange={this.handleChangeFor('firstName')}
                        value={this.state.firstName}
                    />
                </div>
                <div className="reg-form--element reg-form--element__second-name">
                    <TextInput
                        label="ФАМИЛИЯ"
                        handleChange={this.handleChangeFor('secondName')}
                        value={this.state.secondName}
                    />
                </div>
                <div className="reg-form--element">
                    <SelectInput
                        label="ПРОФЕССИЯ"
                        handleChange={this.handleChangeFor('profession')}
                        value={this.state.profession}
                    />
                </div>
                <div className="reg-form--element">
                    <PhoneInput
                        label="ТЕЛЕФОН"
                        handleChange={this.handleChangeFor('phone')}
                        value={this.state.phone}
                    />
                </div>
                <div className="reg-form--submit">
                    <SubmitButton />
                </div>
            </form>
        )
    }
}

export default RegistrationForm